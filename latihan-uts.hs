data Expr   = C Float
            | Expr :+ Expr
            | Expr :- Expr
            | Expr :* Expr
            | Expr :/ Expr
        deriving (Show)

evaluate :: Expr -> Float
evaluate (C x)         = x
evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2

-- (10 * 10 / 2) + (100 + 100 - (1000 / 10)) akan sama hasilnya dengan
-- evaluate ((C 10 :* C 10 :/ 2) :+ (C 100 :+ C 100 :- (C 1000 :/ C 10)))
-- yaitu 240.0

mapExpr :: (Float -> Float) -> Expr -> Expr
mapExpr f (C x)         = C (f x)
mapExpr f (e1 :+ e2)    = (mapExpr f e1) :+ (mapExpr f e2)
mapExpr f (e1 :- e2)    = (mapExpr f e1) :- (mapExpr f e2)
mapExpr f (e1 :* e2)    = (mapExpr f e1) :* (mapExpr f e2)
mapExpr f (e1 :/ e2)    = (mapExpr f e1) :/ (mapExpr f e2)

-- mapExpr (+1) (C 5 :+ C 10)
-- jika dijadikan ekspresi maka C 6.0 :+ C 11.0

foldExpr f v (C x)              = v (C x)
foldExpr f v (e1 :+ e2)         = f ( (foldExpr f v e1) :+ (foldExpr f v e2) )
foldExpr f v (e1 :- e2)         = f ( (foldExpr f v e1) :- (foldExpr f v e2) )
foldExpr f v (e1 :* e2)         = f ( (foldExpr f v e1) :* (foldExpr f v e2) )
foldExpr f v (e1 :/ e2)         = f ( (foldExpr f v e1) :/ (foldExpr f v e2) )

evaluate' e = (\(C x) -> x) (foldExpr f id e)
        where
                f ((C f1) :+ (C f2)) = (C (f1 + f2))
                f ((C f1) :- (C f2)) = (C (f1 - f2))
                f ((C f1) :* (C f2)) = (C (f1 * f2))
                f ((C f1) :/ (C f2)) = (C (f1 / f2))

-- evaluate' ((C 10 :* C 10 :/ 2) :+ (C 100 :+ C 100 :- (C 1000 :/ C 10)))
-- akan sama hasilnya dengan (10 * 10 / 2) + (100 + 100 - (1000 / 10))
-- yaitu 240.0

mapExpr' g = foldExpr f gf
        where
                f x = x
                gf (C x) = (C (g x)) 
                
-- mapExpr' (+1) (C 5 :+ C 10)
-- jika dijadikan ekspresi maka C 6.0 :+ C 11.0


data Expr  = C Float
            | Expr :+ Expr
            | Expr :- Expr
            | Expr :* Expr
            | Expr :/ Expr
            | Let String Expr Expr
            | V String
        deriving (Show)

-- subst v0 e0 (V v1)            = if (v0 == v1) then e0 else (V v1)
-- subst v0 e0 (C c)             = (C c)
-- subst v0 e0 (e1 :+ e2)        = (subst v0 e0 e1) :+ (subst v0 e0 e2)
-- subst v0 e0 (e1 :- e2)        = (subst v0 e0 e1) :- (subst v0 e0 e2)
-- subst v0 e0 (e1 :* e2)        = (subst v0 e0 e1) :* (subst v0 e0 e2)
-- subst v0 e0 (e1 :/ e2)        = (subst v0 e0 e1) :/ (subst v0 e0 e2)
-- subst v0 e0 (Let v1 e1 e2)    = Let v1 e1 (subst v0 e0 e2)

-- evaluate (C x)                = x
-- evaluate (e1 :+ e2)           = evaluate e1 + evaluate e2
-- evaluate (e1 :- e2)           = evaluate e1 - evaluate e2
-- evaluate (e1 :* e2)           = evaluate e1 * evaluate e2
-- evaluate (e1 :/ e2)           = evaluate e1 / evaluate e2
-- evaluate (Let v e0 e1)        = evaluate (subst v e0 e1)
-- evaluate (V v)                = 0.0

-- evaluate (Let "x" (C 10) (Let "y" (C 10) (V "x" :/ V "y" :+ C 1)))
-- menghasilkan 2.0. sama seperti x = 10, y 10, kemudian x/y + 1  

foldExpr f vx (C x)              = vx (C x)
foldExpr f vx (V v)              = vx (V v)
foldExpr f vx (e1 :+ e2)         = f ( (foldExpr f vx e1) :+ (foldExpr f vx e2) )
foldExpr f vx (e1 :- e2)         = f ( (foldExpr f vx e1) :- (foldExpr f vx e2) )
foldExpr f vx (e1 :* e2)         = f ( (foldExpr f vx e1) :* (foldExpr f vx e2) )
foldExpr f vx (e1 :/ e2)         = f ( (foldExpr f vx e1) :/ (foldExpr f vx e2) )
foldExpr f vx (Let v1 e1 e2)     = f ( Let v1 e1 ( f (foldExpr f vx e2) )

substFold v0 e0 = foldExpr f vx
        where
                vx (C x) = C x
                vx (V v) = if (v0 == v) then e0 else (V v)
                f x      = x
 
-- selanjutnya evaluate dapat menggunakan fold

extractX (C x) = x

changeVarToConst = foldExpr f id
        where
                f (Let v e0 e1) = substFold v e0 e1
                f x             = x

evaluate e = extractX (foldExpr f id (changeVarToConst e))
        where
                ff (Let v e0 e1)      = substFold v e0 e1
                ff x                  = x

                f ((C f1) :+ (C f2))  = (C (f1 + f2))
                f ((C f1) :- (C f2))  = (C (f1 - f2))
                f ((C f1) :* (C f2))  = (C (f1 * f2))
                f ((C f1) :/ (C f2))  = (C (f1 / f2))

-- evaluate (Let "x" (C 10) (Let "y" (C 10) (V "x" :/ V "y" :+ C 1)))
-- menghasilkan 2.0. sama seperti x = 10, y 10, kemudian x/y + 1  



-- Implementasi map, menghitung konstanta, menghitung operator dan menghitung variabel
-- Semuanya menggunakan fold

mapExpr = foldExpr id

countConst e = extractX (foldExpr f g (changeVarToConst e))
        where
                g x                   = C 1
                f ((C f1) :+ (C f2))  = C (f1 + f2)
                f ((C f1) :- (C f2))  = C (f1 + f2)
                f ((C f1) :* (C f2))  = C (f1 + f2)
                f ((C f1) :/ (C f2))  = C (f1 + f2)

countOperator e = extractX (foldExpr f g (changeVarToConst e))
        where
                g x                   = C 0
                f ((C f1) :+ (C f2))  = C (f1 + f2 + 1)
                f ((C f1) :- (C f2))  = C (f1 + f2 + 1)
                f ((C f1) :* (C f2))  = C (f1 + f2 + 1)
                f ((C f1) :/ (C f2))  = C (f1 + f2 + 1)

countVariable e = extractX (foldExpr f id (foldExpr f id (mapExpr g e)))
        where
                g (C x)               = C 0
                g (V v)               = C 0
                f (Let v e0 e1)       = C 1 :+ e1
                f ((C f1) :+ (C f2))  = C (f1 + f2)
                f ((C f1) :- (C f2))  = C (f1 + f2)
                f ((C f1) :* (C f2))  = C (f1 + f2)
                f ((C f1) :/ (C f2))  = C (f1 + f2)
