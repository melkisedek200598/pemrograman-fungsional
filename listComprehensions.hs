n1 xs = map (+1) xs

n2 xs ys = concat (map (\x -> map (\y -> (x+y)) ys) xs)

n3 xs = map (+2) (filter (>3) xs)

n4 xys = map (\(x,y) -> x + 3) xys

n5 xys = map (\(x,y) -> x+4) (filter (\(x,y) -> x+y < 5 ) xys)

n6 mxs = map (\(Just x) -> x + 5) mxs

n7 xs = [x+3 | x <- xs]

n8 xs = [x | x <- xs, x > 7]

n9 xs ys = [x+y | x <- xs, y <- ys]

n10 xys = [x+y|(x,y) <- xys, x+y > 3]
