listNaturalNumber x = [1..x]

sumSquares xs = foldr (+) 0 (map (^2) (listNaturalNumber xs))